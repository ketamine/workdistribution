package cracker.executor;

import com.hazelcast.core.*;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class Master implements MembershipListener {

    public static final int MAXIMUM_CAPACITY = 100;
    public static final int SEARCH_UPPER_BOUND = Integer.MAX_VALUE;

    private final Map<String, Set<Integer>> workload = new HashMap<String, Set<Integer>>();
    private final Map<String, Member> members = new HashMap<String, Member>();

    private ReentrantLock lock = new ReentrantLock();
    private BlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>(100);

    private IExecutorService executorService;

    private Integer solution;
    private boolean exhausted;
    private int combinations = 0;

    private String hash;

    public Master(String hash) {
        this.hash = hash;

        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();
        Cluster cluster = hazelcastInstance.getCluster();
        cluster.addMembershipListener(this);
        for (Member member : cluster.getMembers()) {
            if (!cluster.getLocalMember().equals(member)) {
                addMember(member);
            }
        }

        executorService = hazelcastInstance.getExecutorService("BruteForce");

        numberProducerThread.start();
        progressThread.start();
    }

    public static void main(String[] args) throws InterruptedException {
        Integer input = Integer.valueOf(args[0]);
        String md5Hex = DigestUtils.md5Hex(input.toString());
        System.out.println("Cracking " + md5Hex);
        new Master(md5Hex).start();
    }


    private void start() throws InterruptedException {
        while (!exhausted && solution == null) {
            Member worker = chooseWorker();
            Integer input = queue.take();
            String uuid = worker.getUuid();

            lock.lock();

            // I guess it would be a better idea to push the numbers in batches,
            // but I'll go with naive approach for the simplicity's sake
            try {
                Set<Integer> inputs = workload.get(uuid);
                inputs.add(input);
                executorService.submitToMember(new Task(input), worker, new Callback(uuid, input));
            } finally {
                lock.unlock();
            }

        }

        if (solution != null) {
            System.out.println("Solution found! It is " + solution);
        } else {
            System.out.println("Search exhausted");
        }

        System.exit(0);
    }

    private Member chooseWorker() {
        Member worker;
        while ((worker = findAvailableWorker()) == null) {}
        return worker;
    }


    // This part could slow things down since it would be executed in a loop,
    // but we will leave at as is to keep things simple enough
    private Member findAvailableWorker() {
        lock.lock();
        try {
            String candidate = null;
            Integer candidateCapacity = null;

            for (Map.Entry<String, Set<Integer>> entry : workload.entrySet()) {
                int size = entry.getValue().size();
                if (size >= MAXIMUM_CAPACITY) {
                    continue;
                }

                if (candidateCapacity == null || candidateCapacity > size) {
                    candidate = entry.getKey();
                    candidateCapacity = size;
                }
            }

            return candidate != null ? members.get(candidate) : null;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void memberAdded(MembershipEvent membershipEvent) {
        Member newMember = membershipEvent.getMember();
        addMember(newMember);
    }

    private void addMember(Member newMember) {
        String uuid = newMember.getUuid();
        System.out.println("Machine " + uuid + " joined the cluster");

        lock.lock();

        try {
            workload.put(uuid, new HashSet<Integer>());
            members.put(uuid, newMember);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void memberRemoved(MembershipEvent membershipEvent) {
        Member newMember = membershipEvent.getMember();
        String uuid = newMember.getUuid();
        System.out.println("Machine " + uuid + " left the cluster");

        lock.lock();

        try {
            workload.remove(uuid);
            members.remove(uuid);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {}

    private class Callback implements ExecutionCallback<String> {

        private String memberId;
        private Integer input;

        private Callback(String memberId, Integer input) {
            this.memberId = memberId;
            this.input = input;
        }

        @Override
        public void onResponse(String result) {
            lock.lock();

            try {
                combinations++;
                if (result.equals(hash)) {
                    solution = input;
                    return;
                }

                workload.get(memberId).remove(input);
            } finally {
                lock.unlock();
            }
        }

        @Override
        public void onFailure(Throwable t) {
            t.printStackTrace();
        }
    }

    private final Thread progressThread = new Thread() {
        @Override
        public void run() {
            while (true) {
                System.out.printf("Searched through %d combinations \r", combinations);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private final Thread numberProducerThread = new Thread () {

        private Integer currentNumber = 0;

        @Override
        public void run() {
            while (currentNumber < SEARCH_UPPER_BOUND) {
                try {
                    queue.put(currentNumber);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                currentNumber++;
            }

            exhausted = true;
        }
    };

}