package cracker.executor;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.util.concurrent.Callable;

class Task implements Serializable, Callable<String> {

    private Integer input;

    Task(Integer input) {
        this.input = input;
    }

    @Override
    public String call() throws Exception {
        return DigestUtils.md5Hex(input.toString());
    }
}
