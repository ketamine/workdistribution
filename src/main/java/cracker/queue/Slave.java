package cracker.queue;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class Slave {

    private static final long TASK_HANGUP_THRESHOLD = 3000;

    private IQueue<Pair<Integer, String>> resultsQueue;
    private IQueue<Integer> tasksQueue;
    private IQueue<Integer> errorQueue;


    private final ExecutorService executorService = new ThreadPoolExecutor(10, 10, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private final ConcurrentHashMap<Integer, TaskStatus> statusTable = new ConcurrentHashMap<Integer, TaskStatus>();
    private final AtomicLong counter = new AtomicLong();

    public static void main(String[] args) {
        new Slave().start();
    }

    private void start() {
        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();
        tasksQueue = hazelcastInstance.getQueue("tasks");
        resultsQueue = hazelcastInstance.getQueue("results");
        errorQueue = hazelcastInstance.getQueue("errors");
        new Thread(new HashCalculator()).start();
        new Thread(new TaskReaper()).start();
    }

    private class HashCalculator implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    Integer number = tasksQueue.take();
                    Future<?> future = executorService.submit(new Task(number));
                    statusTable.put(number, new TaskStatus(future, System.currentTimeMillis()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class Task implements Runnable {
        private final Integer number;

        public Task(Integer number) {
            this.number = number;
        }

        @Override
        public void run() {
            randomDelay();

            String md5Hex = DigestUtils.md5Hex(number.toString());
            try {
                resultsQueue.put(Pair.of(number, md5Hex));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void randomDelay() {
            if (counter.incrementAndGet() % 10000 == 0) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {}
            }
        }
    }

    private class TaskReaper implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    reap();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        private void reap() throws InterruptedException {
            long currentTime = System.currentTimeMillis();
            Iterator<Map.Entry<Integer, TaskStatus>> iterator = statusTable.entrySet().iterator();

            while (iterator.hasNext()) {
                Map.Entry<Integer, TaskStatus> entry = iterator.next();
                TaskStatus status = entry.getValue();
                Future<?> future = status.getFuture();
                boolean done = future.isDone();
                boolean overdue = currentTime - status.getTimestamp() > TASK_HANGUP_THRESHOLD;
                if (!done && overdue) {
                    future.cancel(true);
                    errorQueue.put(entry.getKey());
                    iterator.remove();
                }

                if (done) {
                    iterator.remove();
                }
            }

        }
    }

    private static class TaskStatus {
        private Future<?> future;
        private long timestamp;

        private TaskStatus(Future<?> future, long timestamp) {
            this.future = future;
            this.timestamp = timestamp;
        }

        public Future<?> getFuture() {
            return future;
        }

        public long getTimestamp() {
            return timestamp;
        }
    }
}
