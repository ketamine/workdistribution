package cracker.queue;

import com.hazelcast.config.Config;
import com.hazelcast.config.QueueConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Master {

    private static final Integer SEARCH_UPPER_BOUND = Integer.MAX_VALUE;
    private final IQueue<Integer> taskQueue;
    private final IQueue<Pair<Integer, String>> resultQueue;
    private final IQueue<Integer> errorQueue;

    private final BlockingQueue<Integer> numberQueue = new LinkedBlockingDeque<Integer>(100);

    private final String hash;
    private boolean exhausted;
    private int combinations;

    private Integer solution;

    public Master(String hash) {
        this.hash = hash;

        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();

        Config config = hazelcastInstance.getConfig();
        QueueConfig queueConfig = config.getQueueConfig("tasks");
        queueConfig.setMaxSize(5000);

        taskQueue = hazelcastInstance.getQueue("tasks");
        resultQueue = hazelcastInstance.getQueue("results");
        errorQueue = hazelcastInstance.getQueue("errors");
        startThreads();
    }

    private void startThreads() {
        new Thread(new NumberProducer()).start();
        new Thread(new ProgressIndicator()).start();
        new Thread(new ResultProcessor()).start();
        new Thread(new ErrorProcessor()).start();
    }

    public static void main(String[] args) throws InterruptedException {
        Integer input = Integer.valueOf(args[0]);
        String md5Hex = DigestUtils.md5Hex(input.toString());
        System.out.println("Cracking " + md5Hex);
        new Master(md5Hex).start();
    }

    private void start() throws InterruptedException {
        while (isActive()) {
            Integer number = numberQueue.take();
            taskQueue.put(number);
        }

        if (solution != null) {
            System.out.println("Solution found! It is " + solution);
        } else {
            System.out.println("Search exhausted");
        }

    }

    private boolean isActive() {
        return !exhausted && solution == null;
    }

    private class NumberProducer implements Runnable {

        private Integer currentNumber = 0;

        @Override
        public void run() {
            while (currentNumber < SEARCH_UPPER_BOUND) {
                currentNumber++;
                try {
                    numberQueue.put(currentNumber);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            exhausted = true;
        }
    }

    private class ResultProcessor implements Runnable {
        @Override
        public void run() {
            while (isActive()) {
                try {
                    processResult();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        private void processResult() throws InterruptedException {
            Pair<Integer, String> result = resultQueue.take();
            combinations++;
            if (result.getValue().equals(hash)) {
                solution = result.getKey();
            }
        }
    }

    private class ErrorProcessor implements Runnable {
        @Override
        public void run() {
            while (isActive()) {
                try {
                    Integer errorNumber = errorQueue.take();
                    System.out.println("Retrying number " + errorNumber);
                    numberQueue.put(errorNumber);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private class ProgressIndicator implements Runnable {
        @Override
        public void run() {
            while (isActive()) {
                System.out.printf("Searched through %d combinations \r", combinations);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {}
            }
        }
    }
}
